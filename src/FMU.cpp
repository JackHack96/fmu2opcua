#include "FMU.h"
#include <chrono>
#include <iostream>
#include <thread>

FMU::FMU(const std::string &path) {
  fmu_cs       = fmi2::fmu(path).as_cs_fmu();
  fmu_md       = fmu_cs->get_model_description();
  fmu_instance = fmu_cs->new_instance();

  for (const auto &i : *fmu_md->model_variables) {
    switch (i.causality) {
      case fmi2::causality::input:
        input_ports.insert(std::pair<fmi2ValueReference, fmi2::scalar_variable>(i.value_reference, i));
        input_values.insert(std::pair<fmi2ValueReference, std::any>(i.value_reference, 0.0));
        break;
      case fmi2::causality::output:
        output_ports.insert(std::pair<fmi2ValueReference, fmi2::scalar_variable>(i.value_reference, i));
        output_values.insert(std::pair<fmi2ValueReference, std::any>(i.value_reference, 0.0));
        break;
      default: break;
    }
  }

  auto def_exp = fmu_md->default_experiment;

  if (def_exp->stepSize.has_value()) step_size = def_exp->stepSize.value();
  if (def_exp->startTime.has_value()) start_time = def_exp->startTime.value();
  if (def_exp->stopTime.has_value()) stop_time = def_exp->stopTime.value();
  if (def_exp->tolerance.has_value()) tolerance = def_exp->tolerance.value();
}

FMU::~FMU() { fmu_instance->terminate(); }

[[noreturn]] void FMU::simulate() {
  while (true) {
    switch (fmu_state) {
      case FMU_RESET:
        fmu_instance->reset();
        fmu_instance->setup_experiment();
        fmu_instance->enter_initialization_mode();
        fmu_instance->exit_initialization_mode();
        if (simulation_type == 0 || simulation_type == 1) fmu_state = FMU_AUTO_RUNNING;
        else
          fmu_state = FMU_MANUAL_RUNNING;
        break;
      case FMU_AUTO_RUNNING:
        updateInputs();
        if (!fmu_instance->step(step_size)) {
          std::cerr << "Error! step() returned with status: " << to_string(fmu_instance->last_status()) << std::endl;
          break;
        }
        updateOutputs();
        if (fmu_instance->get_simulation_time() >= stop_time && simulation_type == 0) fmu_state = FMU_SIMENDED;
        break;
      case FMU_MANUAL_RUNNING:
        if (do_step) {
          updateInputs();
          if (!fmu_instance->step(step_size)) {
            std::cerr << "Error! step() returned with status: " << to_string(fmu_instance->last_status()) << std::endl;
            break;
          }
          do_step = false;
          updateOutputs();
        }
        break;
      case FMU_SIMENDED: break;
    }
    std::this_thread::sleep_for(std::chrono::milliseconds(sim_sleep_ms));
  }
}

void FMU::updateInputs() {
  std::unique_lock<std::mutex> lock(input_mutex);
  for (const auto &i : input_ports) {
    if (i.second.is_real()) {
      if (!fmu_instance->write_real(i.first, std::any_cast<fmi2Real &>(input_values[i.first]))) {
        std::cerr << "Error! write_real() returned with status: " << to_string(fmu_instance->last_status())
                  << std::endl;
        break;
      }
    } else if (i.second.is_integer()) {
      if (!fmu_instance->write_integer(i.first, std::any_cast<fmi2Integer &>(input_values[i.first]))) {
        std::cerr << "Error! write_integer() returned with "
                     "status: "
                  << to_string(fmu_instance->last_status()) << std::endl;
        break;
      }
    } else if (i.second.is_boolean()) {
      if (!fmu_instance->write_boolean(i.first, std::any_cast<fmi2Boolean &>(input_values[i.first]))) {
        std::cerr << "Error! write_boolean() returned with "
                     "status: "
                  << to_string(fmu_instance->last_status()) << std::endl;
        break;
      }
    } else if (i.second.is_string()) {
      if (!fmu_instance->write_string(i.first, std::any_cast<fmi2String &>(input_values[i.first]))) {
        std::cerr << "Error! write_string() returned with "
                     "status: "
                  << to_string(fmu_instance->last_status()) << std::endl;
        break;
      }
    }
  }
}

void FMU::updateOutputs() {
  std::unique_lock<std::mutex> lock(output_mutex);
  for (const auto &i : output_ports) {
    if (i.second.is_real()) {
      fmi2Real value;
      if (!fmu_instance->read_real(i.first, value)) {
        std::cerr << "Error! read_real() returned with status: " << to_string(fmu_instance->last_status()) << std::endl;
        break;
      }
      output_values[i.first] = value;
    } else if (i.second.is_integer()) {
      fmi2Integer value;
      if (!fmu_instance->read_integer(i.first, value)) {
        std::cerr << "Error! read_integer() returned with "
                     "status: "
                  << to_string(fmu_instance->last_status()) << std::endl;
        break;
      }
      output_values[i.first] = value;
    } else if (i.second.is_boolean()) {
      fmi2Boolean value;
      if (!fmu_instance->read_boolean(i.first, value)) {
        std::cerr << "Error! read_boolean() returned with "
                     "status: "
                  << to_string(fmu_instance->last_status()) << std::endl;
        break;
      }
      output_values[i.first] = value;
    } else if (i.second.is_string()) {
      fmi2String value;
      if (!fmu_instance->read_string(i.first, value)) {
        std::cerr << "Error! read_string() returned with status: " << to_string(fmu_instance->last_status())
                  << std::endl;
        break;
      }
      output_values[i.first] = value;
    }
  }
}

void FMU::restartSimulation() { fmu_state = FMU_RESET; }

bool FMU::isSimulating() { return (fmu_state == FMU_AUTO_RUNNING || fmu_state == FMU_MANUAL_RUNNING); }

void FMU::setSimulationSleep(unsigned int time) {
  std::unique_lock<std::mutex> lock(input_mutex);
  sim_sleep_ms = time;
}

double FMU::getSimulationTime() {
  std::unique_lock<std::mutex> lock(output_mutex);
  return fmu_instance->get_simulation_time();
}

void FMU::doSimulationStep() {
  std::unique_lock<std::mutex> lock(input_mutex);
  do_step = true;
}

void FMU::setSimulationStep(double step) {
  std::unique_lock<std::mutex> lock(input_mutex);
  step_size = step;
}

double FMU::getSimulationStep() {
  std::unique_lock<std::mutex> lock(output_mutex);
  return step_size;
}
