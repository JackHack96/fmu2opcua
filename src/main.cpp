#include <csignal>
#include <iostream>
#include <thread>

#include "FMU2OPCUA.h"

FMU2OPCUA *fmu2Opcua;
UA_Boolean running = true;

void stopHandler(int a) {
  UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_USERLAND, "Received Ctrl-C, stopping all...");
  running = false;
}

void print_usage() {
  std::cout << "Usage of FMU2OPCUA:" << std::endl;
  std::cout << "FMU2OPCUA <FMU path> <IP address> <Port> <Polling frequency> <Simulation type>" << std::endl;
  std::cout << "\nDetails about the arguments:" << std::endl;
  std::cout << "- <FMU path>: absolute or relative path of the FMU to simulate" << std::endl;
  std::cout << "- <IP address>: IP address of the OPC UA server (usually 127.0.0.1)" << std::endl;
  std::cout << "- <Port>: port of the OPC UA server (i.e. 4840)" << std::endl;
  std::cout << "- <Polling frequency>: time between every server poll in ms (i.e. 100)" << std::endl;
  std::cout << "- <Simulation type>: number expressing the simulation type" << std::endl;
  std::cout << "The simulation type can be:" << std::endl;
  std::cout << "- 0 perform the simulation according the default experiment stop time" << std::endl;
  std::cout << "- 1 ignore default experiment stop time" << std::endl;
  std::cout << "- 2 performs manual step-by-step simulation (the client use the \"Do step\" port to make the simulation)"
            << std::endl;
}

static void updateValuesCallback(UA_Server *server, void *data) { fmu2Opcua->updateVariables(); }

int main(int argc, char *argv[]) {
  signal(SIGINT, stopHandler);
  signal(SIGTERM, stopHandler);

  if (argc != 6) {
    print_usage();
    return -1;
  }

  std::string fmu_path   = argv[1];
  UA_String server_ip    = UA_String_fromChars(argv[2]);
  UA_UInt16 server_port  = std::stoi(argv[3]);
  UA_UInt32 polling_freq = std::stoi(argv[4]);
  UA_Int16 simulation_type;
  std::istringstream(argv[5]) >> simulation_type;

  std::cout << "Welcome to FMU2OPCUA by Matteo Iervasi!" << std::endl;
  std::cout << "==========================================="
               "==============="
            << std::endl;
  std::cout << "Server will be hosted at " << server_ip.data << ":" << server_port << std::endl;
  std::cout << "FMU to simulate: " << fmu_path << std::endl;
  switch (simulation_type) {
    case 0: std::cout << "Simulation type: Follow default stop time" << std::endl; break;
    case 1: std::cout << "Simulation type: Infinite simulation" << std::endl; break;
    case 2: std::cout << "Simulation type: Manual step-by-step simulation" << std::endl; break;
    default:
      std::cout << "Error, " << simulation_type << " doesn't represent any type of simulation type" << std::endl;
      return -1;
  }

  UA_Server *server       = UA_Server_new();
  UA_ServerConfig *config = UA_Server_getConfig(server);
  UA_ServerConfig_setMinimal(config, server_port, nullptr);
  config->customHostname = server_ip;

  FMU fmu(fmu_path);
  fmu.setSimulationSleep(polling_freq);
  fmu.simulation_type = simulation_type;
  std::thread thr(&FMU::simulate, &fmu);

  fmu2Opcua = new FMU2OPCUA(server, &fmu);

  UA_Server_addRepeatedCallback(server, updateValuesCallback, nullptr, polling_freq, nullptr);

  UA_StatusCode retval = UA_Server_run(server, &running);

  UA_Server_delete(server);
  delete fmu2Opcua;
  return retval == UA_STATUSCODE_GOOD ? EXIT_SUCCESS : EXIT_FAILURE;
}
