#include "FMU2OPCUA.h"
#include <iostream>

FMU2OPCUA::FMU2OPCUA(UA_Server *server, FMU *fmu) : server(server), fmu(fmu) {
  for (const auto &i : fmu->input_ports) {
    if (i.second.is_real()) {
      addVariableReal(i.second.name, i.second.description, std::to_string(i.second.value_reference));
    } else if (i.second.is_integer()) {
      addVariableInteger(i.second.name, i.second.description, std::to_string(i.second.value_reference));
    } else if (i.second.is_boolean()) {
      addVariableBoolean(i.second.name, i.second.description, std::to_string(i.second.value_reference));
    } else if (i.second.is_string()) {
      addVariableString(i.second.name, i.second.description, std::to_string(i.second.value_reference));
    }
    std::cout << "Added " << i.second.name << " to OPC UA server" << std::endl;
  }
  for (const auto &i : fmu->output_ports) {
    if (i.second.is_real()) {
      addVariableReal(i.second.name, i.second.description, std::to_string(i.second.value_reference));
    } else if (i.second.is_integer()) {
      addVariableInteger(i.second.name, i.second.description, std::to_string(i.second.value_reference));
    } else if (i.second.is_boolean()) {
      addVariableBoolean(i.second.name, i.second.description, std::to_string(i.second.value_reference));
    } else if (i.second.is_string()) {
      addVariableString(i.second.name, i.second.description, std::to_string(i.second.value_reference));
    }
    std::cout << "Added " << i.second.name << " to OPC UA server" << std::endl;
  }

  addVariableBoolean("Simulating", "See if the simulation is running", "Simulating");
  addVariableBoolean("Restart", "If true restart the simulation", "Reset");
  addVariableReal("SimulationTime", "This is the simulation time in milliseconds", "SimTime");
  if (fmu->simulation_type == 2) {
    addVariableBoolean("DoStep", "Set to true for performing a simulation step", "DoStep");
    addVariableReal("Simulation Step", "Set the simulation step to perform", "SimStep");
    writeVariableReal("SimStep", fmu->getSimulationStep());
  }
}

FMU2OPCUA::~FMU2OPCUA() = default;

void FMU2OPCUA::updateVariables() {
  if (fmu->simulation_type == 2) {
    if (readVariableBoolean("DoStep")) {
      fmu->setSimulationStep(readVariableReal("SimStep"));
      fmu->doSimulationStep();
    }
  }
  for (const auto &i : fmu->input_ports) {
    if (i.second.is_real()) {
      fmu->input_values[i.first] = readVariableReal(std::to_string(i.first));
    } else if (i.second.is_integer()) {
      fmu->input_values[i.first] = readVariableInteger(std::to_string(i.first));
    } else if (i.second.is_boolean()) {
      fmu->input_values[i.first] = readVariableBoolean(std::to_string(i.first));
    } else if (i.second.is_string()) {
      fmu->input_values[i.first] = readVariableString(std::to_string(i.first));
    }
  }
  for (const auto &i : fmu->output_ports) {
    if (i.second.is_real()) {
      writeVariableReal(std::to_string(i.first), std::any_cast<UA_Double>(fmu->output_values[i.first]));
    } else if (i.second.is_integer()) {
      writeVariableInteger(std::to_string(i.first), std::any_cast<UA_Int32>(fmu->output_values[i.first]));
    } else if (i.second.is_boolean()) {
      writeVariableBoolean(std::to_string(i.first), std::any_cast<UA_Boolean>(fmu->output_values[i.first]));
    } else if (i.second.is_string()) {
      writeVariableString(std::to_string(i.first), std::any_cast<UA_String>(fmu->output_values[i.first]));
    }
  }
  writeVariableBoolean("Simulating", fmu->isSimulating());
  if (readVariableBoolean("Reset")) {
    fmu->restartSimulation();
    writeVariableBoolean("Reset", false);
  }
  writeVariableReal("SimTime", fmu->getSimulationTime());
  if (fmu->simulation_type == 2) writeVariableBoolean("DoStep", false);
}

void FMU2OPCUA::addVariableReal(const std::string &name, const std::string &description, const std::string &nodeid) {
  UA_VariableAttributes attr = UA_VariableAttributes_default;
  UA_Double variable         = 0.0;

  UA_Variant_setScalar(&attr.value, &variable, &UA_TYPES[UA_TYPES_DOUBLE]);

  attr.displayName = UA_LOCALIZEDTEXT(LANG, const_cast<char *>(name.c_str()));
  attr.description = UA_LOCALIZEDTEXT(LANG, const_cast<char *>(description.c_str()));
  attr.dataType    = UA_TYPES[UA_TYPES_DOUBLE].typeId;
  attr.accessLevel = UA_ACCESSLEVELMASK_READ | UA_ACCESSLEVELMASK_WRITE;

  /* Add the variable node to the information model */
  UA_NodeId node_id                  = UA_NODEID_STRING(1, const_cast<char *>(nodeid.c_str()));
  UA_QualifiedName variable_name     = UA_QUALIFIEDNAME(1, const_cast<char *>(name.c_str()));
  UA_NodeId parent_node_id           = UA_NODEID_NUMERIC(0, UA_NS0ID_OBJECTSFOLDER);
  UA_NodeId parent_reference_node_id = UA_NODEID_NUMERIC(0, UA_NS0ID_ORGANIZES);
  UA_Server_addVariableNode(server, node_id, parent_node_id, parent_reference_node_id, variable_name,
                            UA_NODEID_NUMERIC(0, UA_NS0ID_BASEDATAVARIABLETYPE), attr, nullptr, nullptr);
}

void FMU2OPCUA::addVariableInteger(const std::string &name, const std::string &description, const std::string &nodeid) {
  UA_VariableAttributes attr = UA_VariableAttributes_default;
  UA_Int32 variable          = 0;

  UA_Variant_setScalar(&attr.value, &variable, &UA_TYPES[UA_TYPES_INT32]);

  attr.displayName = UA_LOCALIZEDTEXT(LANG, const_cast<char *>(name.c_str()));
  attr.description = UA_LOCALIZEDTEXT(LANG, const_cast<char *>(description.c_str()));
  attr.dataType    = UA_TYPES[UA_TYPES_INT32].typeId;
  attr.accessLevel = UA_ACCESSLEVELMASK_READ | UA_ACCESSLEVELMASK_WRITE;

  /* Add the variable node to the information model */
  UA_NodeId node_id                  = UA_NODEID_STRING(1, const_cast<char *>(nodeid.c_str()));
  UA_QualifiedName variable_name     = UA_QUALIFIEDNAME(1, const_cast<char *>(name.c_str()));
  UA_NodeId parent_node_id           = UA_NODEID_NUMERIC(0, UA_NS0ID_OBJECTSFOLDER);
  UA_NodeId parent_reference_node_id = UA_NODEID_NUMERIC(0, UA_NS0ID_ORGANIZES);
  UA_Server_addVariableNode(server, node_id, parent_node_id, parent_reference_node_id, variable_name,
                            UA_NODEID_NUMERIC(0, UA_NS0ID_BASEDATAVARIABLETYPE), attr, nullptr, nullptr);
}

void FMU2OPCUA::addVariableBoolean(const std::string &name, const std::string &description, const std::string &nodeid) {
  UA_VariableAttributes attr = UA_VariableAttributes_default;
  UA_Boolean variable        = false;

  UA_Variant_setScalar(&attr.value, &variable, &UA_TYPES[UA_TYPES_BOOLEAN]);

  attr.displayName = UA_LOCALIZEDTEXT(LANG, const_cast<char *>(name.c_str()));
  attr.description = UA_LOCALIZEDTEXT(LANG, const_cast<char *>(description.c_str()));
  attr.dataType    = UA_TYPES[UA_TYPES_BOOLEAN].typeId;
  attr.accessLevel = UA_ACCESSLEVELMASK_READ | UA_ACCESSLEVELMASK_WRITE;

  /* Add the variable node to the information model */
  UA_NodeId node_id                  = UA_NODEID_STRING(1, const_cast<char *>(nodeid.c_str()));
  UA_QualifiedName variable_name     = UA_QUALIFIEDNAME(1, const_cast<char *>(name.c_str()));
  UA_NodeId parent_node_id           = UA_NODEID_NUMERIC(0, UA_NS0ID_OBJECTSFOLDER);
  UA_NodeId parent_reference_node_id = UA_NODEID_NUMERIC(0, UA_NS0ID_ORGANIZES);
  UA_Server_addVariableNode(server, node_id, parent_node_id, parent_reference_node_id, variable_name,
                            UA_NODEID_NUMERIC(0, UA_NS0ID_BASEDATAVARIABLETYPE), attr, nullptr, nullptr);
}

void FMU2OPCUA::addVariableString(const std::string &name, const std::string &description, const std::string &nodeid) {
  UA_VariableAttributes attr = UA_VariableAttributes_default;
  UA_String variable         = UA_STRING(const_cast<char *>(""));

  UA_Variant_setScalar(&attr.value, &variable, &UA_TYPES[UA_TYPES_STRING]);

  attr.displayName = UA_LOCALIZEDTEXT(LANG, const_cast<char *>(name.c_str()));
  attr.description = UA_LOCALIZEDTEXT(LANG, const_cast<char *>(description.c_str()));
  attr.dataType    = UA_TYPES[UA_TYPES_STRING].typeId;
  attr.accessLevel = UA_ACCESSLEVELMASK_READ | UA_ACCESSLEVELMASK_WRITE;

  /* Add the variable node to the information model */
  UA_NodeId node_id                  = UA_NODEID_STRING(1, const_cast<char *>(nodeid.c_str()));
  UA_QualifiedName variable_name     = UA_QUALIFIEDNAME(1, const_cast<char *>(name.c_str()));
  UA_NodeId parent_node_id           = UA_NODEID_NUMERIC(0, UA_NS0ID_OBJECTSFOLDER);
  UA_NodeId parent_reference_node_id = UA_NODEID_NUMERIC(0, UA_NS0ID_ORGANIZES);
  UA_Server_addVariableNode(server, node_id, parent_node_id, parent_reference_node_id, variable_name,
                            UA_NODEID_NUMERIC(0, UA_NS0ID_BASEDATAVARIABLETYPE), attr, nullptr, nullptr);
}

void FMU2OPCUA::writeVariableReal(const std::string &nodeid, UA_Double value) {
  UA_NodeId node_id = UA_NODEID_STRING(1, const_cast<char *>(nodeid.c_str()));
  UA_Variant variable;
  UA_Variant_init(&variable);
  UA_Variant_setScalar(&variable, &value, &UA_TYPES[UA_TYPES_DOUBLE]);
  UA_Server_writeValue(server, node_id, variable);
}

void FMU2OPCUA::writeVariableInteger(const std::string &nodeid, UA_Int32 value) {
  UA_NodeId node_id = UA_NODEID_STRING(1, const_cast<char *>(nodeid.c_str()));
  UA_Variant variable;
  UA_Variant_init(&variable);
  UA_Variant_setScalar(&variable, &value, &UA_TYPES[UA_TYPES_INT32]);
  UA_Server_writeValue(server, node_id, variable);
}

void FMU2OPCUA::writeVariableBoolean(const std::string &nodeid, UA_Boolean value) {
  UA_NodeId node_id = UA_NODEID_STRING(1, const_cast<char *>(nodeid.c_str()));
  UA_Variant variable;
  UA_Variant_init(&variable);
  UA_Variant_setScalar(&variable, &value, &UA_TYPES[UA_TYPES_BOOLEAN]);
  UA_Server_writeValue(server, node_id, variable);
}

void FMU2OPCUA::writeVariableString(const std::string &nodeid, UA_String value) {
  UA_NodeId node_id = UA_NODEID_STRING(1, const_cast<char *>(nodeid.c_str()));
  UA_Variant variable;
  UA_Variant_init(&variable);
  UA_Variant_setScalar(&variable, &value, &UA_TYPES[UA_TYPES_STRING]);
  UA_Server_writeValue(server, node_id, variable);
}

UA_Double FMU2OPCUA::readVariableReal(const std::string &nodeid) {
  UA_NodeId node_id = UA_NODEID_STRING(1, const_cast<char *>(nodeid.c_str()));
  UA_Variant variable;
  UA_Server_readValue(server, node_id, &variable);
  return *reinterpret_cast<UA_Double *>(variable.data);
}

UA_Int32 FMU2OPCUA::readVariableInteger(const std::string &nodeid) {
  UA_NodeId node_id = UA_NODEID_STRING(1, const_cast<char *>(nodeid.c_str()));
  UA_Variant variable;
  UA_Server_readValue(server, node_id, &variable);
  return *reinterpret_cast<UA_Int32 *>(variable.data);
}

UA_Boolean FMU2OPCUA::readVariableBoolean(const std::string &nodeid) {
  UA_NodeId node_id = UA_NODEID_STRING(1, const_cast<char *>(nodeid.c_str()));
  UA_Variant variable;
  UA_Server_readValue(server, node_id, &variable);
  return *reinterpret_cast<UA_Boolean *>(variable.data);
}

UA_String FMU2OPCUA::readVariableString(const std::string &nodeid) {
  UA_NodeId node_id = UA_NODEID_STRING(1, const_cast<char *>(nodeid.c_str()));
  UA_Variant variable;
  UA_Server_readValue(server, node_id, &variable);
  return *reinterpret_cast<UA_String *>(variable.data);
}
